use ArtHW1
create table SUBJECT
(
��SUBJ_ID�� NUMERIC not null,
��SUBJ_NAME VARCHAR(60),
��HOUR����� NUMERIC,
��SEMESTER�� NUMERIC
);
alter table SUBJECT
��add primary key (SUBJ_ID);

create table UNIVERSITY
(
��UNIV_ID�� NUMERIC not null,
��UNIV_NAME VARCHAR(160),
��RATING��� NUMERIC,
��CITY����� VARCHAR(60)
);
alter table UNIVERSITY
��add primary key (UNIV_ID);

create table LECTURER
(
��LECTURER_ID NUMERIC not null,
��SURNAME���� VARCHAR(60),
��NAME������� VARCHAR(60),
��CITY������� VARCHAR(60),
��UNIV_ID���� NUMERIC
);
alter table LECTURER
��add primary key (LECTURER_ID);
alter table LECTURER
��add constraint UNIVLECT_FOR_KEY foreign key (UNIV_ID)
��references UNIVERSITY (UNIV_ID);

create table STUDENT
(
��STUDENT_ID NUMERIC not null,
��SURNAME��� VARCHAR(60),
��NAME������ VARCHAR(60),
��STIPEND��� NUMERIC(16,2),
��KURS������ NUMERIC,
��CITY������ VARCHAR(60),
��BIRTHDAY�� DATE,
��UNIV_ID��� NUMERIC
);
alter table STUDENT
��add primary key (STUDENT_ID);
alter table STUDENT
��add constraint UNIV_FOR_KEY foreign key (UNIV_ID)
��references UNIVERSITY (UNIV_ID);

create table EXAM_MARKS
(
��EXAM_ID��� NUMERIC not null,
��STUDENT_ID NUMERIC not null,
��SUBJ_ID��� NUMERIC not null,
��MARK������ NUMERIC,
��EXAM_DATE� DATE
);
alter table EXAM_MARKS
��add primary key (EXAM_ID,STUDENT_ID,SUBJ_ID);
alter table EXAM_MARKS
��add constraint STUDENT_FOR_KEY foreign key (STUDENT_ID)
��references STUDENT (STUDENT_ID);
alter table EXAM_MARKS
��add constraint SUBJECT_FOR_KEY foreign key (SUBJ_ID)
��references SUBJECT (SUBJ_ID);
create index STUDENT_ID_1 on EXAM_MARKS (STUDENT_ID);

create table SUBJ_LECT
(
��LECTURER_ID NUMERIC not null,
��SUBJ_ID���� NUMERIC not null
);
alter table SUBJ_LECT
��add primary key (LECTURER_ID,SUBJ_ID);
alter table SUBJ_LECT
��add constraint LECT_FOR_KEY foreign key (LECTURER_ID)
��references LECTURER (LECTURER_ID);
alter table SUBJ_LECT
��add constraint SUBJ_FOR_KEY foreign key (SUBJ_ID)
��references SUBJECT (SUBJ_ID);


-- RESTORE DATABASE

DELETE FROM EXAM_MARKS;
DELETE FROM STUDENT;
DELETE FROM SUBJ_LECT;
DELETE FROM LECTURER;
DELETE FROM SUBJECT;
DELETE FROM UNIVERSITY;

INSERT INTO UNIVERSITY ([UNIV_ID] ,[UNIV_NAME] ,[RATING] ,[CITY]) VALUES (1001, 'BRU',  300, 'Mogilev');
INSERT INTO UNIVERSITY ([UNIV_ID] ,[UNIV_NAME] ,[RATING] ,[CITY]) VALUES (1002, 'BGU',  500, 'Minsk');
INSERT INTO UNIVERSITY ([UNIV_ID] ,[UNIV_NAME] ,[RATING] ,[CITY]) VALUES (1003, 'MGUP',  200, 'Mogilev');
INSERT INTO UNIVERSITY ([UNIV_ID] ,[UNIV_NAME] ,[RATING] ,[CITY]) VALUES (1004, 'BGPU',  400, 'Minsk');
INSERT INTO UNIVERSITY ([UNIV_ID] ,[UNIV_NAME] ,[RATING] ,[CITY]) VALUES (1005, 'BIP',  100, 'Mogilev');
INSERT INTO UNIVERSITY ([UNIV_ID] ,[UNIV_NAME] ,[RATING] ,[CITY]) VALUES (1006, 'MIT',  500, 'Boston');

INSERT INTO LECTURER ([LECTURER_ID] ,[SURNAME] ,[NAME] ,[CITY] ,[UNIV_ID]) VALUES (2001, 'Petrov',  'Petr', 'Minsk', 1002);
INSERT INTO LECTURER ([LECTURER_ID] ,[SURNAME] ,[NAME] ,[CITY] ,[UNIV_ID]) VALUES (2002, 'Ivanov',  'Ivan', 'Mogilev', 1001);
INSERT INTO LECTURER ([LECTURER_ID] ,[SURNAME] ,[NAME] ,[CITY] ,[UNIV_ID]) VALUES (2003, 'Sidorov',  'Ivan', 'Minsk', 1004);
INSERT INTO LECTURER ([LECTURER_ID] ,[SURNAME] ,[NAME] ,[CITY] ,[UNIV_ID]) VALUES (2004, 'Domrachev',  'Petr', 'Minsk', 1002);
INSERT INTO LECTURER ([LECTURER_ID] ,[SURNAME] ,[NAME] ,[CITY] ,[UNIV_ID]) VALUES (2005, 'Kozlov',  'Petr', 'Mogilev', 1003);
INSERT INTO LECTURER ([LECTURER_ID] ,[SURNAME] ,[NAME] ,[CITY] ,[UNIV_ID]) VALUES (2006, 'Svetlov',  'Marat', 'Minsk', 1004);
INSERT INTO LECTURER ([LECTURER_ID] ,[SURNAME] ,[NAME] ,[CITY] ,[UNIV_ID]) VALUES (2007, 'Korzhov',  'Sergei', 'Mogilev', 1001);
INSERT INTO LECTURER ([LECTURER_ID] ,[SURNAME] ,[NAME] ,[CITY] ,[UNIV_ID]) VALUES (2008, 'Korzhov',  'Petr', 'Mogilev', 1005);

INSERT INTO SUBJECT ([SUBJ_ID] ,[SUBJ_NAME] ,[HOUR] ,[SEMESTER]) VALUES (3001, 'Programming',  200, 2);
INSERT INTO SUBJECT ([SUBJ_ID] ,[SUBJ_NAME] ,[HOUR] ,[SEMESTER]) VALUES (3002, 'History',  100, 2);
INSERT INTO SUBJECT ([SUBJ_ID] ,[SUBJ_NAME] ,[HOUR] ,[SEMESTER]) VALUES (3003, 'English',  150, 2);
INSERT INTO SUBJECT ([SUBJ_ID] ,[SUBJ_NAME] ,[HOUR] ,[SEMESTER]) VALUES (3004, 'Math',  250, 1);
INSERT INTO SUBJECT ([SUBJ_ID] ,[SUBJ_NAME] ,[HOUR] ,[SEMESTER]) VALUES (3005, 'Russian',  100, 1);
INSERT INTO SUBJECT ([SUBJ_ID] ,[SUBJ_NAME] ,[HOUR] ,[SEMESTER]) VALUES (3006, 'Economic',  150, 3);
INSERT INTO SUBJECT ([SUBJ_ID] ,[SUBJ_NAME] ,[HOUR] ,[SEMESTER]) VALUES (3007, 'Administration',  100, 2);
INSERT INTO SUBJECT ([SUBJ_ID] ,[SUBJ_NAME] ,[HOUR] ,[SEMESTER]) VALUES (3008, 'Modeling',  150, 4);

INSERT INTO SUBJ_LECT ([LECTURER_ID], [SUBJ_ID]) VALUES (2001, 3001)
INSERT INTO SUBJ_LECT ([LECTURER_ID], [SUBJ_ID]) VALUES (2002, 3001)
INSERT INTO SUBJ_LECT ([LECTURER_ID], [SUBJ_ID]) VALUES (2003, 3005)
INSERT INTO SUBJ_LECT ([LECTURER_ID], [SUBJ_ID]) VALUES (2004, 3004)
INSERT INTO SUBJ_LECT ([LECTURER_ID], [SUBJ_ID]) VALUES (2004, 3008)
INSERT INTO SUBJ_LECT ([LECTURER_ID], [SUBJ_ID]) VALUES (2006, 3002)
INSERT INTO SUBJ_LECT ([LECTURER_ID], [SUBJ_ID]) VALUES (2007, 3002)
INSERT INTO SUBJ_LECT ([LECTURER_ID], [SUBJ_ID]) VALUES (2001, 3007)
INSERT INTO SUBJ_LECT ([LECTURER_ID], [SUBJ_ID]) VALUES (2002, 3007)
INSERT INTO SUBJ_LECT ([LECTURER_ID], [SUBJ_ID]) VALUES (2003, 3003)
INSERT INTO SUBJ_LECT ([LECTURER_ID], [SUBJ_ID]) VALUES (2004, 3006)
INSERT INTO SUBJ_LECT ([LECTURER_ID], [SUBJ_ID]) VALUES (2005, 3002)
INSERT INTO SUBJ_LECT ([LECTURER_ID], [SUBJ_ID]) VALUES (2006, 3001)

INSERT INTO STUDENT ([STUDENT_ID] ,[SURNAME] ,[NAME] ,[STIPEND] ,[KURS] ,[CITY] ,[BIRTHDAY] ,[UNIV_ID]) VALUES (4001, 'Student1', 'Ivan', 100, 1, 'Minsk', '01.01.1995', 1002)
INSERT INTO STUDENT ([STUDENT_ID] ,[SURNAME] ,[NAME] ,[STIPEND] ,[KURS] ,[CITY] ,[BIRTHDAY] ,[UNIV_ID]) VALUES (4002, 'Student2', 'Dmitri', 150, 2, 'Minsk', '02.01.1994', 1002)
INSERT INTO STUDENT ([STUDENT_ID] ,[SURNAME] ,[NAME] ,[STIPEND] ,[KURS] ,[CITY] ,[BIRTHDAY] ,[UNIV_ID]) VALUES (4003, 'Student3', 'Sergei', 100, 1, 'Minsk', '05.11.1995', 1002)
INSERT INTO STUDENT ([STUDENT_ID] ,[SURNAME] ,[NAME] ,[STIPEND] ,[KURS] ,[CITY] ,[BIRTHDAY] ,[UNIV_ID]) VALUES (4004, 'Student4', 'Pavel', 135, 2, 'Minsk', '11.11.1994', 1002)
INSERT INTO STUDENT ([STUDENT_ID] ,[SURNAME] ,[NAME] ,[STIPEND] ,[KURS] ,[CITY] ,[BIRTHDAY] ,[UNIV_ID]) VALUES (4005, 'Student5', 'Eugeny', 100, 1, 'Minsk', '08.01.1995', 1004)
INSERT INTO STUDENT ([STUDENT_ID] ,[SURNAME] ,[NAME] ,[STIPEND] ,[KURS] ,[CITY] ,[BIRTHDAY] ,[UNIV_ID]) VALUES (4006, 'Student6', 'Victor', 100, 1, 'Minsk', '01.05.1995', 1004)
INSERT INTO STUDENT ([STUDENT_ID] ,[SURNAME] ,[NAME] ,[STIPEND] ,[KURS] ,[CITY] ,[BIRTHDAY] ,[UNIV_ID]) VALUES (4007, 'Student7', 'Vitaly', 100, 1, 'Minsk', '01.01.1995', 1004)
INSERT INTO STUDENT ([STUDENT_ID] ,[SURNAME] ,[NAME] ,[STIPEND] ,[KURS] ,[CITY] ,[BIRTHDAY] ,[UNIV_ID]) VALUES (4008, 'Student11', 'Ivan', 100, 1, 'Mogilev', '01.01.1995', 1003)
INSERT INTO STUDENT ([STUDENT_ID] ,[SURNAME] ,[NAME] ,[STIPEND] ,[KURS] ,[CITY] ,[BIRTHDAY] ,[UNIV_ID]) VALUES (4009, 'Student12', 'Dmitri', 100, 1, 'Mogilev', '02.01.1995', 1003)
INSERT INTO STUDENT ([STUDENT_ID] ,[SURNAME] ,[NAME] ,[STIPEND] ,[KURS] ,[CITY] ,[BIRTHDAY] ,[UNIV_ID]) VALUES (4010, 'Student13', 'Sergei', 130, 2, 'Mogilev', '05.11.1994', 1003)
INSERT INTO STUDENT ([STUDENT_ID] ,[SURNAME] ,[NAME] ,[STIPEND] ,[KURS] ,[CITY] ,[BIRTHDAY] ,[UNIV_ID]) VALUES (4011, 'Student14', 'Pavel', 100, 1, 'Mogilev', '11.11.1995', 1003)
INSERT INTO STUDENT ([STUDENT_ID] ,[SURNAME] ,[NAME] ,[STIPEND] ,[KURS] ,[CITY] ,[BIRTHDAY] ,[UNIV_ID]) VALUES (4012, 'Student15', 'Eugeny', 100, 2, 'Mogilev', '08.01.1994', 1001)
INSERT INTO STUDENT ([STUDENT_ID] ,[SURNAME] ,[NAME] ,[STIPEND] ,[KURS] ,[CITY] ,[BIRTHDAY] ,[UNIV_ID]) VALUES (4013, 'Student16', 'Victor', 100, 2, 'Mogilev', '01.05.1993', 1001)
INSERT INTO STUDENT ([STUDENT_ID] ,[SURNAME] ,[NAME] ,[STIPEND] ,[KURS] ,[CITY] ,[BIRTHDAY] ,[UNIV_ID]) VALUES (4014, 'Student17', 'Vitaly', 100, 1, 'Mogilev', '01.01.1991', 1001)
INSERT INTO STUDENT ([STUDENT_ID] ,[SURNAME] ,[NAME] ,[STIPEND] ,[KURS] ,[CITY] ,[BIRTHDAY] ,[UNIV_ID]) VALUES (4015, 'Student17', 'Vitaly', 100, 1, 'Mogilev', '01.01.1991', 1006)



INSERT INTO EXAM_MARKS  ([EXAM_ID] ,[STUDENT_ID] ,[SUBJ_ID] ,[MARK] ,[EXAM_DATE]) VALUES (5001, 4001, 3001, 10, '01.03.2017')
INSERT INTO EXAM_MARKS  ([EXAM_ID] ,[STUDENT_ID] ,[SUBJ_ID] ,[MARK] ,[EXAM_DATE]) VALUES (5002, 4002, 3001, 9, '01.03.2017')
INSERT INTO EXAM_MARKS  ([EXAM_ID] ,[STUDENT_ID] ,[SUBJ_ID] ,[MARK] ,[EXAM_DATE]) VALUES (5003, 4003, 3001, 6, '01.03.2017')
INSERT INTO EXAM_MARKS  ([EXAM_ID] ,[STUDENT_ID] ,[SUBJ_ID] ,[MARK] ,[EXAM_DATE]) VALUES (5004, 4004, 3001, 8, '01.03.2017')
INSERT INTO EXAM_MARKS  ([EXAM_ID] ,[STUDENT_ID] ,[SUBJ_ID] ,[MARK] ,[EXAM_DATE]) VALUES (5005, 4001, 3002, 10, '03.03.2017')
INSERT INTO EXAM_MARKS  ([EXAM_ID] ,[STUDENT_ID] ,[SUBJ_ID] ,[MARK] ,[EXAM_DATE]) VALUES (5006, 4002, 3002, 10, '03.03.2017')
INSERT INTO EXAM_MARKS  ([EXAM_ID] ,[STUDENT_ID] ,[SUBJ_ID] ,[MARK] ,[EXAM_DATE]) VALUES (5007, 4003, 3002, 7, '03.03.2017')
INSERT INTO EXAM_MARKS  ([EXAM_ID] ,[STUDENT_ID] ,[SUBJ_ID] ,[MARK] ,[EXAM_DATE]) VALUES (5008, 4004, 3002, 8, '03.03.2017')
INSERT INTO EXAM_MARKS  ([EXAM_ID] ,[STUDENT_ID] ,[SUBJ_ID] ,[MARK] ,[EXAM_DATE]) VALUES (5001, 4012, 3003, 10, '01.03.2017')
INSERT INTO EXAM_MARKS  ([EXAM_ID] ,[STUDENT_ID] ,[SUBJ_ID] ,[MARK] ,[EXAM_DATE]) VALUES (5002, 4009, 3003, 9, '01.03.2017')
INSERT INTO EXAM_MARKS  ([EXAM_ID] ,[STUDENT_ID] ,[SUBJ_ID] ,[MARK] ,[EXAM_DATE]) VALUES (5003, 4010, 3003, 6, '01.03.2017')
INSERT INTO EXAM_MARKS  ([EXAM_ID] ,[STUDENT_ID] ,[SUBJ_ID] ,[MARK] ,[EXAM_DATE]) VALUES (5004, 4011, 3003, 8, '01.03.2017')
INSERT INTO EXAM_MARKS  ([EXAM_ID] ,[STUDENT_ID] ,[SUBJ_ID] ,[MARK] ,[EXAM_DATE]) VALUES (5005, 4012, 3004, 10, '03.03.2017')
INSERT INTO EXAM_MARKS  ([EXAM_ID] ,[STUDENT_ID] ,[SUBJ_ID] ,[MARK] ,[EXAM_DATE]) VALUES (5006, 4011, 3004, 5, '03.03.2017')
INSERT INTO EXAM_MARKS  ([EXAM_ID] ,[STUDENT_ID] ,[SUBJ_ID] ,[MARK] ,[EXAM_DATE]) VALUES (5007, 4010, 3004, 7, '03.03.2017')
INSERT INTO EXAM_MARKS  ([EXAM_ID] ,[STUDENT_ID] ,[SUBJ_ID] ,[MARK] ,[EXAM_DATE]) VALUES (5008, 4009, 3004, 6, '03.03.2017')
INSERT INTO EXAM_MARKS  ([EXAM_ID] ,[STUDENT_ID] ,[SUBJ_ID] ,[MARK] ,[EXAM_DATE]) VALUES (5008, 4005, 3004, 6, '03.03.2017')



-- 1) ���� �� �������������� ����������� ������������� ������� � ����������� �����?
   /* 1) � ������� STUDENT ����� ������� � ��������� ������� ������� CITY � STIPEND (3� ��)
      2)� ������� UNIVERSITY ����� ������� ������� CITY � ��������� id, � �������� LEXTURER ����� ������� ���� �����
	  3)� ������� 'SUBJECT' ����� �������� id � name  � ������� ������� ���������� �  SUBJECT_id, semestr, hours
	  4)������� EXAM_MARKS ����� ������� �� EXAM � MARKS - ������ - exam_id, subj_id, 'date' ������ - student_id,exam_id, mark*/

-- 2) ������� ���� ������� ��������� � �������������� �� �������������
   SELECT STUDENT.SURNAME,LECTURER.SURNAME 
	   FROM STUDENT, LECTURER
	   WHERE STUDENT.UNIV_ID = LECTURER.UNIV_ID
-- 3) ������� ���������� ���� ��������� (ID) ������� ���������� ������ 
   SELECT DISTINCT A.STUDENT_ID, B.STUDENT_ID
       FROM EXAM_MARKS A
	   JOIN EXAM_MARKS B
	   ON A.MARK = B.MARK AND A.EXAM_ID = b.EXAM_ID AND A.STUDENT_ID < B.STUDENT_ID

-- 4) ������� ������ � ������������ � ������������� �������� �� ��������� � ID �������� � ��������, �������� ����������� ����� ������� ��� ������ ������ � �������
   SELECT *
   FROM
   (SELECT t.*, E.STUDENT_ID, E.MARK,
   CASE 
   WHEN E.MARK =t.MAX THEN 'max'
   WHEN E.MARK =t.MIN THEN 'min' ELSE null END AS result

   FROM EXAM_MARKS E 
   JOIN (SELECT SUBJ_ID, MAX(MARK) AS MAX, MIN(MARK) AS MIN
   FROM EXAM_MARKS
   GROUP BY SUBJ_ID) AS t
   ON E.SUBJ_ID=t.SUBJ_ID) AS Q
   WHERE Q.result is not null			
   
-- 5) ������� ������ ���������� � ��������� � �� ���������, ���� ������� �� ������ �������, ������� nulls
   SELECT S.STUDENT_ID, S.SURNAME,S.NAME, S.STIPEND,S.KURS,S.CITY, S.BIRTHDAY,S.UNIV_ID, E.EXAM_ID, E.SUBJ_ID, E.MARK, E.EXAM_DATE 
	 FROM STUDENT S
	 LEFT JOIN  EXAM_MARKS E
	 ON S.STUDENT_ID = E.STUDENT_ID

-- 6) ������� ������ ���������� � ��������� � �� ���������, ������ ��� ���������, ��������� ��������
   SELECT S.STUDENT_ID, S.SURNAME,S.NAME, S.STIPEND,S.KURS,S.CITY, S.BIRTHDAY,S.UNIV_ID, E.EXAM_ID, E.SUBJ_ID, E.MARK, E.EXAM_DATE 
	 FROM STUDENT S
	 JOIN  EXAM_MARKS E
	 ON S.STUDENT_ID = E.STUDENT_ID
-- 7) ������� ������ ���������� � ���������, �� ������� ��� ���� ���� ���� ������
   SELECT S.SUBJ_ID,S.SUBJ_NAME, S.HOUR, S.SEMESTER 
	FROM SUBJECT S
	JOIN EXAM_MARKS E
	ON S.SUBJ_ID = E.SUBJ_ID

-- 8) ������� �������� ��������� � ������� ����. � ���� ������� �� �������� �� ��������, �� ������� 0
   SELECT S.SUBJ_NAME, ISNULL(AVG(E.MARK), '0')
	   FROM SUBJECT S
	   LEFT JOIN EXAM_MARKS E
	   ON S.SUBJ_ID = E.SUBJ_ID
	   GROUP BY S.SUBJ_NAME
-- 9) ������� ������� �������� ������� ����� ����� ���� ��������� (2 �������)
   -- ������ ������
   SELECT distinct L.SURNAME
   FROM LECTURER L
   JOIN SUBJ_LECT st  
   on st.LECTURER_ID = L.LECTURER_ID and st.LECTURER_ID in (SELECT st.LECTURER_ID
                                                            FROM SUBJ_LECT st
														    group by st.LECTURER_ID
														    having COUNT(*)>2)
    --������ ������
   SELECT distinct L.SURNAME
   FROM LECTURER L, SUBJ_LECT st  
   where l.LECTURER_ID = st.LECTURER_ID and st.LECTURER_ID in (SELECT st.LECTURER_ID
                                                               FROM SUBJ_LECT st
														       group by st.LECTURER_ID
														       having COUNT(*)>2)
-- 10) ������� ������: ������� �������� ��� �������������, ID ������������ � ����������: '���� ������������� � ���� ��������', '���� ������������� � ��� ���������', '���� �������� � ���� �������������', '���� ��������  � ��� �������������'
   SELECT distinct L.SURNAME, U.UNIV_ID,
	CASE 
	WHEN L.SURNAME IS NOT NULL AND S.SURNAME IS NOT NULL THEN '���� ������������� � ���� ��������'
	WHEN L.SURNAME IS NOT NULL AND S.SURNAME IS NULL THEN '���� ������������� � ��� ���������'
    END WHOYESWHONO
	FROM UNIVERSITY U
	LEFT JOIN STUDENT S
	ON U.UNIV_ID = S.UNIV_ID
	LEFT JOIN LECTURER L
	ON U.UNIV_ID = L.UNIV_ID
	GROUP BY U.UNIV_ID, S.SURNAME,L.SURNAME
	HAVING L.SURNAME IS NOT NULL 
	UNION 
	SELECT distinct S.SURNAME, U.UNIV_ID,
	CASE 
	WHEN S.SURNAME IS NOT NULL AND L.SURNAME IS NOT NULL THEN '���� �������� � ���� �������������'
	WHEN S.SURNAME IS NOT NULL AND L.SURNAME IS NULL THEN '���� ��������  � ��� �������������'
    END WHOYESWHONO
	FROM UNIVERSITY U
	LEFT JOIN STUDENT S
	ON U.UNIV_ID = S.UNIV_ID
	LEFT JOIN LECTURER L
	ON U.UNIV_ID = L.UNIV_ID
	GROUP BY U.UNIV_ID, S.SURNAME,L.SURNAME
	HAVING S.SURNAME IS NOT NULL 
	
-- 11) ������� ������� � ������� � ������ ������� �� ������������� �� �����: ID ������������, ����, ������� ����
   SELECT U.UNIV_ID, EM.EXAM_DATE, AVG(EM.MARK) MARKS
	INTO BEST_MARKS
	FROM EXAM_MARKS EM
	JOIN UNIVERSITY U
	ON (SELECT UNIV_ID 
	    FROM STUDENT
		WHERE STUDENT_ID = EM.STUDENT_ID) = U.UNIV_ID  
	GROUP BY EM.EXAM_DATE,U.UNIV_ID

-- 12) ��������� �� 1003 'MGUP' ��������� � 1004 'BGPU' (�������� ����� � ������������� � �����)
   UPDATE STUDENT 
   SET UNIV_ID = 1004, 
   CITY = (SELECT CITY 
           FROM UNIVERSITY 
		   WHERE UNIVERSITY.UNIV_ID = 1004)
   WHERE UNIV_ID = 1003

-- 13) ������� ���������� �� �������������, � ������� ��� �� ������ ��������
   SELECT U.UNIV_ID,U.UNIV_NAME, U.CITY, U.RATING 
   FROM UNIVERSITY U
   LEFT JOIN STUDENT S
   ON U.UNIV_ID = S.UNIV_ID
   WHERE S.UNIV_ID IS NULL 

-- 14) ������� ����, ID �������� � ���������� ������� ���������, ��� ������� ��� � ���� ���� �� �������� �������� ������ 4 ������
   SELECT E.EXAM_DATE, SUBJ_ID, COUNT(S.STUDENT_ID) AMMOUNT
   FROM EXAM_MARKS E
   JOIN STUDENT S
   ON S.STUDENT_ID = E.STUDENT_ID 
   GROUP BY E.EXAM_DATE,E.SUBJ_ID
   HAVING E.SUBJ_ID IN (SELECT E.SUBJ_ID
          FROM EXAM_MARKS E
		  GROUP BY  E.EXAM_DATE,E.SUBJ_ID
		  HAVING COUNT(MARK) > 4)

-- 15) ������� ������ ���������� � ��������� � ����������� ����� ����� 150 � 200, ����� ������ � ��������� 3 � 4
  SELECT * FROM SUBJECT
  WHERE HOUR BETWEEN 150 AND 200 
        AND SEMESTER NOT IN (3,4)