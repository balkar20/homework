/**
 * На основании функции-конструктора из предыдущего задания
 * необходимо вынести все функции-методы в prototype функции-конструктора Hamburger
 *
 * @constructor
 * @param size        Размер
 * @param stuffing    Начинка
 */
var Hamburger = function () {
    var calcSumPrice = function (sumAcc, curEl){
        return sumAcc + curEl.price;
    }
    var calcSumCaloriies = function (sumAcc, curEl) {
        return sumAcc + curEl.calories;
    }
    var HamburgerConstructor = function (size, stuffing) {
        this.size = size;
        this.stuffings = [stuffing];
        this.toppings = [];
    }
    HamburgerConstructor.prototype.addStuffing = function (stuffing) {
        if (this.stuffings.length <= this.size.maxSize && this.stuffings.indexOf(stuffing) < 0) {
            this.stuffings.push(stuffing);
        }
    };

    HamburgerConstructor.prototype.addTopping = function (topping) {
        if (this.toppings.indexOf(topping) === -1) {
            this.toppings.push(topping);
        }
    };

    HamburgerConstructor.prototype.removeTopping = function (topping) {
        var index = this.toppings.indexOf(topping);
        if (index >= 0) {
            this.toppings.splice(index, 1)
        }
    };


    HamburgerConstructor.prototype.getSize = function () {
        return this.size.name;
    };

    HamburgerConstructor.prototype.getStuffing = function () {
        return this.stuffings.map(function  (elem) {
            return elem.name;
        });
    };

    HamburgerConstructor.prototype.getTopping = function () {
        return this.toppings.map(function (elem) {
            return elem.name;
        });
    };

    HamburgerConstructor.prototype.calculateCalories = function () {
        var calories = this.size.calories;
        calories += this.toppings.reduce(calcSumCaloriies, 0) + this.stuffings.reduce(calcSumCaloriies, 0);

        return calories;
    };

    HamburgerConstructor.prototype.calculatePrice = function () {
        var price = this.size.startPrice;
        price += this.toppings.reduce(calcSumPrice, 0) + this.stuffings.reduce(calcSumPrice, 0);

        return price;
    };

    return HamburgerConstructor;
}();

Hamburger.STUFFING_CHEESE = {name: "Cheese", calories: 25, price: 0.1};
Hamburger.STUFFING_CHICKEN = {name: "Chicken", calories: 23, price: 0.15};
Hamburger.STUFFING_POTATO = {name: "Potato", calories: 27, price: 0.2};
Hamburger.STUFFING_SALAD = {name: "Salad", calories: 23, price: 0.1};
Hamburger.STUFFING_ONION = {name: "Onion", calories: 23, price: 0.1};
Hamburger.STUFFING_PAPRICA = {name: "Paprica", calories: 28, price: 0.3};

Hamburger.TOPPING_MAYO = {name: "Mayo", calories: 21, price: 0.2};
Hamburger.TOPPING_SPICE = {name: "Spice", calories: 24, price: 0.3};
Hamburger.TOPPING_KETCHUP = {name: "Ketchup", calories: 21, price: 0.1};
Hamburger.TOPPING_SOURCREAM = {name: "sour cream", calories: 22, price: 0.3};
Hamburger.TOPPING_TOMATO = {name: "Tomato", calories: 21, price: 0.2};

Hamburger.SIZE_SMALL = {name: "Small", calories: 200, maxSize: 5, startPrice: 0.7};
Hamburger.SIZE_LARGE = {name: "Large", calories: 300, maxSize: 10, startPrice: 1.5};

var hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
hamburger.addTopping(Hamburger.TOPPING_TOMATO);
hamburger.addTopping(Hamburger.TOPPING_SOURCREAM);
hamburger.addTopping(Hamburger.TOPPING_KETCHUP);

hamburger.addStuffing(Hamburger.STUFFING_POTATO);
hamburger.addStuffing(Hamburger.STUFFING_ONION);
hamburger.addStuffing(Hamburger.STUFFING_PAPRICA);

hamburger.removeTopping(Hamburger.TOPPING_TOMATO);

console.log(hamburger.getStuffing());
console.log(hamburger.getTopping());
console.log(hamburger.calculateCalories());
