var CLASS_CONNECTOR_LINE = "tree-connector-line";
var CLASS_HIERARCHY_LINE = "tree-hierarchy-line";
var CLASS_CONTAINER = "tree-wrapper";
var CLASS_DIRECT_HEADER = "header";
var CLASS_DIRECT_SIZE = "size";
var UL_CLASS = "tree";

/**
 * Функция для генерации структуры иерархии
 * в виде дерева каталогов и файлов.
 */
function generateTree() {
    var treeContainer = document.getElementById("tree");
    var resourses = document.getElementsByClassName("resource");
    var objs = parseResoursesToObjects(resourses);
    buildHtmlHierarchy(objs, "ROOT", treeContainer);
}

var Resourse = function (name, type, size, location) {
    this.name = name;
    this.type = type;
    this.location = location;
    this.size = size;

    this.getElementsWithThisLocation = function (objs) {
        var arr = objs.filter(function (el) {
            return this.name === el.location;
        });
        return arr;
    };
};

function parseResoursesToObjects(resourses) {
    var objs = [];
    for (var i = 0; i < resourses.length; i++) {
        objs.push(parseResToObject(resourses[i]));
    }
    return objs;
}

function parseResToArr(res) {
    var resTexts = getLiTexts(res);
    var arr = [];

    for (var i = 0; i < resTexts.length; i++) {
        var text = resTexts[i].replace(/ +/g, " ");
        var val = text.split(' ')[1];
        arr.push(val);
    }
    return arr;
}

function parseResToObject(res) {
    var arr = parseResToArr(res);
    var name = getResourseName(res);
    var type = arr[0];
    var size = arr[1];
    var location = arr[2];
    return new Resourse(name, type, size, location);
}

function getResourseName(res) {
    return res.firstElementChild.textContent;
}

function getLiTexts(resource) {
    var arr = [];
    var lies = resource.getElementsByTagName('li');
    for (var i = 0; i < lies.length; i++) {
        var textContent = lies[i].firstElementChild.textContent;
        var two = textContent + lies[i].textContent.replace(textContent, '').replace(/\r|\n/g, '');
        arr.push(two);
    }
    return arr;
}

function deleteObjectsFromArray(arrFrom, arrOfDel) {
    return arrFrom.filter(function (el) {
        return arrOfDel.indexOf(el) === -1;
    });
}

function drawDivWrapperForElement() {
    var container = document.createElement("div");
    container.setAttribute("class", CLASS_CONTAINER);
    var hirLineContainer = document.createElement("div");
    hirLineContainer.setAttribute("class", CLASS_HIERARCHY_LINE);
    var ulElemennt = document.createElement("ul");
    ulElemennt.setAttribute("class", UL_CLASS);
    container.appendChild(hirLineContainer);
    container.appendChild(ulElemennt);
    return container;
}

function drawLiElementForObject(resourse) {
    var liContainer = document.createElement("li");
    var treeConnector = document.createElement("span");
    treeConnector.className = CLASS_CONNECTOR_LINE;
    var header = document.createElement("span");
    header.textContent = " " + resourse.name.toLowerCase();
    var sizeText = formatSize(resourse.size);
    if (resourse.type === "F") {
        header.textContent += " " + "[" + sizeText + "]";
        header.className = CLASS_DIRECT_SIZE;
    } else {
        header.className = CLASS_DIRECT_HEADER;
    }


    liContainer.appendChild(treeConnector);
    liContainer.appendChild(header);
    return liContainer;
}

function formatSize(size) {
    var arrSizes = [];
    arrSizes[0] = "B";
    arrSizes[1] = "Kb";
    arrSizes[2] = "Mb";
    arrSizes[3] = "Gb";
    arrSizes[4] = "Tb";
    var metric = 0;
    while (size >= 1024) {
        metric++;
        size /= 1024;
    }
    var result = Math.round(size) + arrSizes[metric];
    return result;
}

function buildHtmlHierarchy(resourses, rootName, container) {
    var isRoot = rootName === "ROOT";
    if (isRoot && container.children.length === 0) {
        var header = document.createElement("span");
        header.className = CLASS_DIRECT_HEADER;
        header.textContent = rootName;
        container.appendChild(header);
        container.appendChild(drawDivWrapperForElement());
    } else if (container.children.length === 2) {
        container.appendChild(drawDivWrapperForElement());
    }
    var filtred = resourses.filter(function (el) {
        return el.location === rootName;
    });
    var updatedResourses = deleteObjectsFromArray(resourses, filtred);
    if (filtred.length !== 0) {
        filtred.forEach(function (el) {
            var rootWrapper = container.lastElementChild;
            var rootUl = rootWrapper.lastElementChild;
            var liChild;
            if (el.getElementsWithThisLocation(resourses)) {
                rootUl.insertBefore(drawLiElementForObject(el), rootUl.firstElementChild);
                liChild = rootUl.firstElementChild;
            } else {
                rootUl.appendChild(drawLiElementForObject(el));
                liChild = rootUl.lastElementChild;
            }
            buildHtmlHierarchy(updatedResourses, el.name, liChild);
        });
    }
}