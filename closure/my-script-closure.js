/**
 * Реализовать две функции repeat, sequence
 *
 * Напиши функцию создания генератора sequence(start, step).
 * Она при вызове возвращает другую функцию-генератор,
 * которая при каждом вызове дает число на 1 больше, и так до бесконечности.
 *
 * Начальное число, с которого начинать отсчет, и шаг, задается при создании генератора.
 * Шаг можно не указывать, тогда он будет равен одному. Начальное значение по умолчанию равно 0.
 * Генераторов можно создать сколько угодно.
 *
 * @param start - начальное число, с которого начинать отсчет.
 *                 Начальное значение по умолчанию равно 0.
 * @param step -  шаг, задается при создании генератора.
 *                Шаг можно не указывать, тогда он будет равен одному
 *
 * @return function - вызове возвращает другую функцию-генератор,
 * которая при каждом вызове дает число начиная с start на step больше, и так до бесконечности
 */
function sequence(start, step) {
    step = step || 1;
    start = start || 0;

    start -= step;
    return function () {
        return start += step;
    }
}

/**
 * Функция вызвает функцию func заданное число (times) раз
 *
 * @param func - функция, которая будет вызываться
 * @param times - сколько раз нужно вызвать функцию func
 */
function repeat(fun, n) {
    for (var i = 0; i < n; i++) {
        console.log(fun());
    }
}

var generator = sequence(10, 3);
var generator2 = sequence(0, 2);

console.log(generator()); // 10
console.log(generator()); // 13

console.log(generator2()); // 0

repeat(generator2, 5); // [2, 4, 6, 8, 10]

/**
 * Реализовать конструктор для созадния гамбургером с описанными методами ниже.
 * Необходимо оформить с помощью шаблона "Модуль".
 *
 * Класс, объекты которого описывают параметры гамбургера.
 *
 * @constructor
 * @param size        Размер
 * @param stuffing    Начинка
 */

var Hamburger = function () {
    var calcSumPrice = function (sumAcc, curEl) {
      return sumAcc + curEl.price;
    };

    var calcSumCaloriies = function (sumAcc, curEl) {
      return sumAcc + curEl.calories;
    };

    function HamburgerConstuctor(size, stuffing) {
        var thisSize = size;
        var stuffings = [stuffing];
        var toppings = [];
        /**
         * Добавить начинку к гамбургеру. Можно добавить несколько
         * добавок, при условии, что они разные.
         *
         * Нельзя добавить начинку, если размер амбургера
         * Hamburger.SIZE_SMALL и кол-во начинку равно 5.
         *
         * Если размер гамбургера Hamburger.SIZE_LARGE,
         * можно добавлять не больше 10 начинку
         *
         * @param stuffing  Тип начинку
         */
        this.addStuffing = function (stuffing) {
            if (stuffings.length <= thisSize.maxSize && stuffings.indexOf(stuffing) < 0) {
                stuffings.push(stuffing);
            }
        };

        /**
         * Добавить топпинг к гамбургеру. Можно добавить несколько,
         * при условии, что они разные.
         *
         * @param topping  Тип топпинга
         */
        this.addTopping = function (topping) {
            if (toppings.indexOf(topping) === -1) {
                toppings.push(topping);
            }
        };

        /**
         * Убрать топпинг, при условии, что он ранее был
         * добавлен.
         *
         * @param topping Тип топпинга
         */
        this.removeTopping = function (topping) {
            var index = toppings.indexOf(topping);
            if (index >= 0) {
                toppings.splice(index, 1);
            }
        };

        /**
         * Узнать размер гамбургера
         * @return {Number} размер гамбургера
         */
        this.getSize = function () {
            return thisSize;
        };

        /**
         * Узнать начинку гамбургера
         * @return {Array} Массив добавленных начинок, содержит константы
         * Hamburger.STUFFING_*
         */
        this.getStuffing = function () {
            return stuffings.slice();
        };

        /**
         * Получить список добавок
         *
         * @return {Array} Массив добавленных добавок, содержит константы
         * Hamburger.TOPPING_*
         */
        this.getToppings = function () {
            return toppings.slice();
        };

        /**
         * Узнать калорийность
         * @return {Number} Калорийность в калориях
         */
        this.calculateCalories = function () {
            var calories = thisSize.calories;
            calories += toppings.reduce(calcSumCaloriies, 0) + stuffings.reduce(calcSumCaloriies, 0);

            return calories;
        };

        /**
         * Узнать цену гамбургера
         * @return {Number} Цена гамбургера
         */
        this.calculatePrice = function () {
            var price = thisSize.startPrice;
            price += toppings.reduce(calcSumPrice, 0) + stuffings.reduce(calcSumPrice, 0);

            return price;
        };

    }
    return HamburgerConstuctor;
}();
/* Размеры, виды начинок и добавок
* Можно добавить свои топпинги и начинки
*
* Размеры начинаются с SIZE_*
* Начинки начинаются с STUFFING_*
* Топпинги начинаются с TOPPING_*
*/
Hamburger.STUFFING_CHEESE = {name: "Cheese", calories: 25, price: 0.1};
Hamburger.STUFFING_CHICKEN = {name: "Chicken", calories: 23, price: 0.15};
Hamburger.STUFFING_POTATO = {name: "Potato", calories: 27, price: 0.2};
Hamburger.STUFFING_SALAD = {name: "Salad", calories: 23, price: 0.1};
Hamburger.STUFFING_ONION = {name: "Onion", calories: 23, price: 0.1};
Hamburger.STUFFING_PAPRICA = {name: "Paprica", calories: 28, price: 0.3};

Hamburger.TOPPING_MAYO = {name: "Mayo", calories: 21, price: 0.2};
Hamburger.TOPPING_SPICE = {name: "Spice", calories: 24, price: 0.3};
Hamburger.TOPPING_KETCHUP = {name: "Ketchup", calories: 21, price: 0.1};
Hamburger.TOPPING_SOURCREAM = {name: "sour cream", calories: 22, price: 0.3};
Hamburger.TOPPING_TOMATO = {name: "Tomato", calories: 21, price: 0.2};

Hamburger.SIZE_SMALL = {name: "Small", calories: 200, maxSize: 5, startPrice: 0.7};
Hamburger.SIZE_LARGE = {name: "Large", calories: 300, maxSize: 10, startPrice: 1.5};

// Пример использования

// маленький гамбургер с начинкой из сыра
var hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
// добавим из майонеза
hamburger.addTopping(Hamburger.TOPPING_MAYO);

// добавим картофель
hamburger.addStuffing(Hamburger.STUFFING_POTATO);

// спросим сколько там калорий
console.log('Калории: ', hamburger.calculateCalories());

// сколько стоит
console.log('Цена: ', hamburger.calculatePrice());

// я тут передумал и решил добавить еще приправу
hamburger.addTopping(Hamburger.TOPPING_SPICE);

// А сколько теперь стоит?
console.log('Цена с соусом ', hamburger.calculatePrice());

// большой ли гамбургер получился?
console.log('Большой ли гамбургер? ', hamburger.getSize() === Hamburger.SIZE_SMALL); // -> false

// убрать добавку
hamburger.removeTopping(Hamburger.TOPPING_SPICE);

console.log('Сколько топпингов добавлено ', hamburger.getToppings().length); // 1
